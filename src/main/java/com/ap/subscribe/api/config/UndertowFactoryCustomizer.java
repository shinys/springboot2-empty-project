package com.ap.subscribe.api.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;

/**
 * https://docs.spring.io/spring-boot/docs/current/reference/html/howto-embedded-web-servers.html
 */
@Component
public class UndertowFactoryCustomizer implements WebServerFactoryCustomizer<UndertowServletWebServerFactory> {
    private static final Logger logger = LogManager.getLogger(UndertowFactoryCustomizer.class);

    @Override
    public void customize(UndertowServletWebServerFactory factory) {
        int ioThreads = Math.max(Runtime.getRuntime().availableProcessors(), 2) * 4;
        int workerThreads = ioThreads * 8;
        int bufferSize = 512;
        boolean directBuffers = false;
        long maxMemory = Runtime.getRuntime().maxMemory();

        if (maxMemory < 64 * 1024 * 1024) {
            directBuffers = false;
            bufferSize = 512;
        } else if (maxMemory < 128 * 1024 * 1024) {
            directBuffers = true;
            bufferSize = 1024;
        } else {
            directBuffers = true;
            //generally the max amount of data that can be sent in a single write() call
            bufferSize = 1024 * 16;
        }

        factory.setBufferSize(bufferSize);
        factory.setUseDirectBuffers(directBuffers);
        factory.setIoThreads(ioThreads);
        factory.setWorkerThreads(workerThreads);
        factory.setAccessLogEnabled(true);
        int finalBufferSize = bufferSize;
        boolean finalDirectBuffers = directBuffers;
        logger.info("Undertow Configuration. buffer:{}bytes, directBuffer:{}, ioThreads:{}, workerThreads:{}",
                ()-> finalBufferSize,()-> finalDirectBuffers,()->ioThreads,()->workerThreads );
    }
}
