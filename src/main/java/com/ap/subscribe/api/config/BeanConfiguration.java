package com.ap.subscribe.api.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

@Configuration
public class BeanConfiguration {
    private final static Logger logger = LogManager.getLogger(BeanConfiguration.class);
    private static final AtomicLong atomicLong = new AtomicLong(1);

    @Primary
    @Bean
    public ObjectMapper objectMapper() {
        return JsonMapper.builder()
                //.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"))
                .enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS,
                        DeserializationFeature.USE_LONG_FOR_INTS,
                        DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,
                        DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT,
                        DeserializationFeature.READ_ENUMS_USING_TO_STRING,
                        DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
                .enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)
                .enable(JsonGenerator.Feature.IGNORE_UNKNOWN)
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .serializationInclusion(JsonInclude.Include.NON_NULL)
                .propertyNamingStrategy(PropertyNamingStrategy.LOWER_CAMEL_CASE)
                .addModule(new JavaTimeModule())   // java8 DateTime 형식 지원. ObjectMappper version 2용.  version 3는 최하 java버전이 8이라 불필요.
                .build()
                ;
    }

    @Primary
    @Bean(destroyMethod = "shutdownNow")
    public ScheduledExecutorService schedule() {
        return Executors.newScheduledThreadPool(60,
                r -> getThread(r)
        );
    }

    @Primary
    @Bean(destroyMethod = "shutdownNow")
    public ExecutorService executor() {
        return new ThreadPoolExecutor(
                40,
                40,
                0L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>(1000),
                r -> getThread(r),
                new ThreadPoolExecutor.CallerRunsPolicy()
        );
    }

    private Thread getThread(Runnable r) {
        Thread t = new Thread(r);
        t.setName(r.getClass().getName() + ":" + atomicLong.getAndIncrement());
        t.setDaemon(true);
        t.setPriority(Thread.NORM_PRIORITY);
        t.setUncaughtExceptionHandler((t2, e) -> {
            logger.error(()-> t2.getName(),e);
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        });
        return t;
    }
}
