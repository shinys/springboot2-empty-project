package com.ap.subscribe.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.servlet.Filter;

/**
 * Spring Security 설정
 * https://spring.io/guides/topicals/spring-security-architecture
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * 스프링 시큐리티 룰
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable() // API 서버이므로 인증폼 사용하지 않음.
                .csrf().disable()      // rest api 서버이므로 csrf 사용하지 않음.
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)  // API 서버이므로 세션이 불피요한 무상태 어플리케이션
                .and()
                    .authorizeRequests().anyRequest().permitAll()
                .and()
                    // 스프링 시큐리티 필터 순서 : https://codevang.tistory.com/275
                    .addFilterBefore(characterEncodingFilter(), CsrfFilter.class);
                // TODO api 문서는 인증 후 열람 가능하도록 설정 필요.
    }

    /**
     * 스프링 시큐리티 룰 무시할 path
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .mvcMatchers("/favicon.ico")
                .antMatchers("/v3/api-docs","/api-docs","/swagger-resources/**","/swagger","/swagger-ui.html","/webjars/**")
                .antMatchers("/resources/**","/css/**","/js/**","/statics/**");
    }

    private static Filter characterEncodingFilter (){
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        return filter;
    }
}
