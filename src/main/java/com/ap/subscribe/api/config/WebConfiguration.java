package com.ap.subscribe.api.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * WebMvcConfigurer 구현을 통해 @EnableWebMvc 어노테이션이 자동으로 설정해주는 세팅에
 * 사용자가 원하는 세팅을 추가할 수 있다.
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    /**
     * CORS 설정
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/api/**")
                .allowedOrigins("*")
                .allowedMethods(
                        HttpMethod.GET.name()
                        , HttpMethod.POST.name()
                        , HttpMethod.PUT.name()
                        , HttpMethod.DELETE.name())
                .allowCredentials(false)
                .maxAge(3600);
    }

    /**
     * 정적 리소스 경로 설정
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }
}
