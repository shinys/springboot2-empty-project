package com.ap.subscribe.api.preflight;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RequiredArgsConstructor
@RestController
public class HelloController {

    private final HelloRepository helloRepository;

    @RequestMapping(path = "/hello/{no}",method = RequestMethod.GET)
    public ResponseEntity<HelloEntity> retrieve(
            @PathVariable(name = "no", required = true) Long no
    ){
      return ResponseEntity.ok(helloRepository.findById(no).orElseThrow(()-> new NoSuchElementException("")));
    }

    @RequestMapping(path = "/hello",method = RequestMethod.POST)
    public ResponseEntity<HelloEntity> create(
            @RequestBody HelloEntity hello
    ){
        return ResponseEntity.ok(helloRepository.save(hello));
    }


    @RequestMapping(path = "/hello",method = RequestMethod.PUT)
    public ResponseEntity<HelloEntity> update(
            @RequestBody HelloEntity hello
    ){
        return ResponseEntity.ok(helloRepository.save(hello));
    }


    @RequestMapping(path = "/hello",method = RequestMethod.DELETE)
    public ResponseEntity delete(
            @PathVariable(name = "no", required = true) Long no
    ){
        helloRepository.deleteById(no);
        return ResponseEntity.noContent().build();
    }
}
