package com.ap.subscribe.api.preflight;

import com.querydsl.core.types.Predicate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
public class HelloRepositoryTest {
    private HelloRepository helloRepository;

    @Autowired
    public HelloRepositoryTest(HelloRepository helloRepository){
        this.helloRepository = helloRepository;
    }

    public void init(){
        HelloEntity entity = new HelloEntity();
        entity.setMessage("안녕");

        helloRepository.save(entity);
    }

    @Test
    public void save(){
        HelloEntity entity = new HelloEntity();
        entity.setMessage("하이룽");

        helloRepository.save(entity);

        Assertions.assertNotNull(entity.getNo());
    }


    @Test
    public void predicate(){
        init();
        QHelloEntity entity = QHelloEntity.helloEntity;
        final Predicate predicate = entity.no.eq(1L);

        Assertions.assertEquals(1, helloRepository.count(predicate));
    }
}
