###  Springboot2 기반 Rest API 깡통 어플리케이션


#### 어플리케이션 구성 요소
* 자바 : 8 이상
* Gradle : 6.8   
* 프레임워크 : springboot 2.4, spring-data-jpa with querydsl      
* 웹서버 : Undertow    
* Http Client : webclient  
* 테스트 : jupiter  
* 문서화 : openapi 3.0 (swagger)  

## Developer Settings  
소스를 포함한 모든 text file 인코딩은 UTF-8으로 설정.   
> 예외로 .properties 파일은 IOS-8859-1 임.

![IntelliJ Setting](./document/intellij-file-encoding.png)  

#### build  
`$ gradlew clean build`  


#### run  
`$ java -jar -Dfile.encoding=utf-8 ap-subscribe-api.jar`  

`$ gradlew bootRun`


#### 참고
##### 스프링 시큐리티  
https://spring.io/guides/topicals/spring-security-architecture  
스프링 시큐리티 필터 순서   
https://codevang.tistory.com/275  



##### WebMvcConfigurer 구현을 통해 @EnableWebMvc 애노테이션이 자동으로 설정해주는 세팅에 더해 추가 설정 가능  
~~~java
package com.ap.subscribe.api.config;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {
~~~


##### Lombok & QueryDSL annotation process    
Gradle 4.6 이후 설정 방식이 대폭 변경됨.  이 깡통 프로젝트에서는 QClass가 생성되는 위치를 지정하는 sourceSet까지는 설정하지 않음.   
https://netframework.tistory.com/search/querydsl     
https://qiita.com/kakasak/items/ae6a1335d68e06172d4d   
~~~properties
compile 'com.querydsl:querydsl-core'
compile 'com.querydsl:querydsl-sql'
compile "com.querydsl:querydsl-sql-spring:${dependencyManagement.importedProperties['querydsl.version']}"
compile "com.querydsl:querydsl-jpa:${dependencyManagement.importedProperties['querydsl.version']}"
annotationProcessor(
        "com.querydsl:querydsl-apt:${dependencyManagement.importedProperties['querydsl.version']}:jpa",   //querydsl JPAAnnotationProcessor 사용 지정
        "org.hibernate.javax.persistence:hibernate-jpa-2.1-api:1.0.2.Final",
        "javax.annotation:javax.annotation-api:1.3.2"
)
compile 'org.projectlombok:lombok'
annotationProcessor 'org.projectlombok:lombok'
~~~

##### WebClient
Springboot2 에서도 여전히 RestTemplate 을 사용할 수 있으나 더 이상의 기능 추가는 없으며
non-blocking 모드를 지원하는 WebClient 로 전환을 가이드 하고 있으므로
httpclient로 WebClient를 @Bean으로 등록 함.


##### springdoc-openapi swagger 3 설정
spring rest doc vs openapi  : https://www.baeldung.com/spring-rest-docs-vs-openapi  
